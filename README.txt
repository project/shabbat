Module: Shabbat Keeper 
Author: Amnon Levav <http://www.levavie.com>

Description
=================
Google friendly way to allow Jewish websites to become more kosher by closing them on shabbat

Requirements
=================
Saturday Gaurd user account.

Installation
=================
Copy the 'shabbat' module directory in to your Drupal modules directory as usual, install it and enable it.

Usage
=================

1. Configure the module's setting. Register a demo account at the shabbat service provider site, then add your user ID to the admin/settings/shabbat page.

2. Enable the block provided by the module for all pages which need to keep shabbat and yom tov. Pages on which the block appears will be blocked on shabbat and yom tov. This allows you to block only a certain part of the site. 

Advanced Settings
=================

Use the following code anywhere on your template files (especially, on
page.tpl.php) to plant the sabbath settings in a a specific locations as
needed.

<?php print theme_shabbat_code(); ?>
